## Résumé

*   Ajouter au benchmark le nombre de développeurs et quel type (doctorant, job à temps 
    plein, etc...)

*   Le benchmark se fait sur la difficulté à implémenter un modèle ML non-présent dans le
    framework à partir de layers présents dans le framework, sans trop mettre les mains à
    la pate.

*   Trouver deux modèles ML _communs_ et mesurer les deux:

        * un déjà implémenté
        * un pas implémenté

*   Se focus sur les projets qui supportent les FPGAs en sortie.
