###############################################################################
# File: leflow.py
#
# Created by: Lucas Elisei <lucas.elisei@master.hes-so.ch>
# Created on: 26.05.2019
#
# LeFlow implementation for a MLP 784(Input)-128(HiddenLayer1)-64(HiddenLayer2)
#   -10(Output) model.
###############################################################################

# Import libraries.
import numpy as np
import tensorflow
from tensorflow.examples.tutorials.mnist import input_data

import sys
sys.path.append('../../src')
import processMif as mif

# Load MNIST dataset.
mnist_data = input_data.read_data_sets('MNIST_data/', one_hot=True)

# Number of inputs and outputs.
num_input  = 784    # 28*28 pixels
num_output = 10     # 0-9 digits

# Number of neurons for each hidden layers.
num_layers_0 = 128
num_layers_1 = 64

# Initialisation parameters.
learning_rate_init = 0.001
regularizer_rate   = 0.1

# Placeholder for input data.
input_x = tensorflow.placeholder(tensorflow.float32, shape=(None, num_input), name="input_x")
input_y = tensorflow.placeholder(tensorflow.float32, shape=(None, num_output), name="input_y")
# Dropout layer.
keep_prob = tensorflow.placeholder(tensorflow.float32)

# Initialise weights and biases to 0.
weights_0 = tensorflow.Variable(tensorflow.zeros([num_input, num_layers_0]))
biases_0  = tensorflow.Variable(tensorflow.zeros([num_layers_0]))

weights_1 = tensorflow.Variable(tensorflow.zeros([num_layers_0, num_layers_1]))
biases_1  = tensorflow.Variable(tensorflow.zeros([num_layers_1]))

weights_2 = tensorflow.Variable(tensorflow.zeros([num_layers_1, num_output]))
biases_2  = tensorflow.Variable(tensorflow.zeros([num_output]))

# Define hidden layers.
hidden_output_0   = tensorflow.nn.relu(tensorflow.matmul(input_x, weights_0) + biases_0)
hidden_output_0_0 = tensorflow.nn.dropout(hidden_output_0, keep_prob)

hidden_output_1   = tensorflow.nn.relu(tensorflow.matmul(hidden_output_0_0, weights_1) + biases_1)
hidden_output_1_1 = tensorflow.nn.dropout(hidden_output_1, keep_prob)

predicted_y       = tensorflow.matmul(hidden_output_1_1, weights_2) + biases_2

# Define the loss function.
loss = tensorflow.reduce_mean(tensorflow.nn.softmax_cross_entropy_with_logits_v2(logits=predicted_y, labels=input_y))   \
    + regularizer_rate * (tensorflow.reduce_sum(tensorflow.square(biases_0)) + tensorflow.reduce_sum(tensorflow.square(biases_1)))

# Variable learning rate.
learning_rate = tensorflow.train.exponential_decay(learning_rate_init, 0, 5, 0.85, staircase=True)

# Adam optimizer for finding the correct weight.
optimizer = tensorflow.train.AdamOptimizer(learning_rate)
train_step = optimizer.minimize(loss, var_list=[weights_0, weights_1, weights_2,
                                                biases_0, biases_1, biases_2])

# Initialisation
init = tensorflow.global_variables_initializer()

with tensorflow.Session() as session:
    epochs = 1000
    session.run(init)

    for _ in range(epochs):
        batch_x, batch_y = mnist_data.train.next_batch(100)
        session.run(train_step, feed_dict={input_x: batch_x, input_y: batch_y})

    correct_prediction = tensorflow.equal(tensorflow.argmax(input_y, 1), tensorflow.argmax(input_y, 1))
    accuracy = tensorflow.reduce_mean(tensorflow.cast(correct_prediction, tensorflow.float32))
    net_accuracy = session.run(accuracy, feed_dict={input_x: mnist_data.test.images, input_y: mnist_data.test.labels})

    print('The accuracy over the MNIST dataset is {:.2f}%'.format(net_accuracy * 100))

    # Generating hardware.
    with tensorflow.device('device:XLA_CPU:0'):
        y = tensorflow.matmul(hidden_output_1_1, weights_2)[0] + biases_2
    session.run(y, {input_x: [mnist_data.test.images[123]]})

    # Creating memories for testing.
    test_image = 123
    mif.createMem([biases_0.eval(), weights_0.eval(), biases_1.eval(), weights_1.eval(), biases_2.eval(), weights_2.eval(), mnist_data.test.images[test_image]])

    # Print expected result.
    print('Expected result: {}'.format(str(np.argmax(mnist_data.test.labels[test_image]))))