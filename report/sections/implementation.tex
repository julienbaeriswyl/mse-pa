\section{Implementation}

Now that we chose the models we want to implement and the frameworks we will
use to deploy them on an FPGA, we can proceed to the implementation and
benchmarking the results.

Since this research focuses on benchmarking the full development and deployment
process, we will document the installation procedure, the ease of implementation
and the output results.

\textbf{Note:} For this section, all the installation and implementation steps
have been executed on a machine running Linux.

\subsection{TVM}

\subsubsection{Installation}

This section describes how to build and install the TVM packages from scratch.

It consists of two steps:

\begin{enumerate}
\item   Build the \texttt{libtvm.so} shared library from the C++ codes
\item   Install the Python packages
\end{enumerate}

To get started, clone TVM's repo from Github:

\begin{minted}{bash}
git clone --recursive https://github.com/dmlc/tvm
\end{minted}

\subsubsection*{Build the shared library}

First, we need to install required packages:

\begin{minted}{bash}
sudo apt-get update
sudo apt-get install -y python python-dev python-setuptools gcc libtinfo-dev zlib1g-dev build-essential cmake
\end{minted}

First, create a build directory and copy the \texttt{cmake/config.cmake} file to the directory:

\begin{minted}{bash}
mkdir build
cp cmake/config.cmake build
\end{minted}

We also need to install LLVM. The TVM official documentation says that LLVM 4.0
or higher is required but we recommend using LLVM 8.0 since there seems to be
issues with lower versions. To install LLVM, we can either build it from source
(takes long time) or we can download a pre-built version from
\textit{LLVM Download Page}\footnotemark.

\footnotetext{\url{http://releases.llvm.org/download.html}}

To let CMake know that we use LLVM, we need to modify \texttt{build/config.cmake}:

\begin{enumerate}
\item   If you downloaded pre-built LLVM binaries, add \\
        \texttt{set(USE\_LLVM /path/to/your/llvm/bin/llvm-config)}
\item   If you installed LLVM system-wide, you can let CMake search for a usable
        version of LLVM by adding \texttt{set(USE\_LLVM ON)}
\end{enumerate}

We can then build TVM and its related libraries:

\begin{minted}{bash}
cd build
cmake ..
make -j4
\end{minted}

\subsubsection*{Python packages installation}

For this section, we \textbf{heavily} recommend using a \textit{Conda environment}
to ensure that no harm is done to the Python system-wide installation.

Install TVM Python packages:

\begin{minted}{bash}
cd python; python setup.py install --user; cd ..
cd topi/python; python setup.py install --user; cd ../..
cd nnvm/python; python setup.py install --user; cd ../..
\end{minted}

Install Python dependencies:

\begin{minted}{bash}
pip install --user numpy decorator attrs tornado psutil xgboost
\end{minted}

Everything should now be setup correctly! As you can see, TVM installation is
quite straightforward and can be performed in a few minutes by experienced
users, making it a really interesting framework.

\subsubsection{Board setup}

Now that all the required software has been installed, we need to setup the
\acrshort{fpga} board. Since the TVM documentation uses the Pynq-Z1 board for
tutorials, the following steps target the Pynq-Z1 \cite{pynq}. A microSD card
with a capacity of at least 8 GB and an Ethernet cable are also required. To be
able to talk to the board, make sure to assign your computer a static IP address
(in this paper, we assume the computer has the IP address \texttt{192.168.1.4}
and the Pynq has the IP address \texttt{192.168.2.99}).

The first step is to download the latest Pynq image (PYNQ-Z1 v2.4 at the time
of writing), which can be found on the \textit{Pynq Development Board}
page\footnotemark.

\footnotetext{\url{http://www.pynq.io/board.html}}

To make sure the Pynq board is properly setup, power it on and try to connect to
it:

\begin{minted}{bash}
ssh xilinx@192.168.2.99
\end{minted}

\textbf{Note:} By default, the Pynq board uses \texttt{xilinx} as username and
password.

Now that the Pynq board is setup, we need to build and deploy an RPC server so
that TVM can communicate with it.

Because the direct board-to-computer connection prevents the board from directly
accessing the internet, we'll need to mount the Pynq's filesystem to your
computer filesystem with \texttt{sshfs}. Next, we clone the TVM Github repository
into the \texttt{sshfs} mountpoint on your computer.

\begin{minted}{bash}
# On the computer
mkdir /path/to/mountpoint
sshfs xilinx@192.168.2.99:/home/xilinx /path/to/mountpoint
cd /path/to/mountpoint
git clone --recursive https://github.com/dmlc/tvm
cd ~
sudo umount /path/to/mountpoint
\end{minted}

Now that we have cloned the TVM/VTA repository in the Pynq's filesystem, we can
\texttt{ssh} into it and launch the build of the TVM-based RPC server.

\begin{minted}{bash}
ssh xilinx@192.168.2.99
# Build TVM runtime library
cd /home/xilinx/tvm
mkdir build
cp cmake/config.cmake build/
# Copy Pynq specific configuration
cp vta/config/pynq_sample.json build/vta_config.json
cd build
cmake ..
make runtime vta -j2    # (takes ~5 min)
\end{minted}

We now have to create an RPC tracker on the computer and then bind the Pynq on
it:

\begin{minted}{bash}
# On the computer
python -m tvm.exec.rpc_tracker --host=0.0.0.0 --port=9190
# On the PYNQ board
python3 -m tvm.exec.rpc_server --tracker 192.168.2.4:9190 --key=pynq
\end{minted}

\todo{include commands for RPC server, both computer and pynq}

You should see the following line being displayed when starting the RPC server:

\begin{minted}{text}
INFO:root:RPCServer: bind to 0.0.0.0:9091
\end{minted}

In order to let the RPC server run, you will need to leave the RPC server
running in an \texttt{ssh} session.

\subsubsection{Implementation}

The file \texttt{src/run\_tvm.py} contains the code required to run the implementation.
In this section, we will explain step by step what this code does.

The first step is to fetch the network workload. The function
\mintinline{python}{get_network()} does that. It takes the network name as input
and get the workload according to the batch size, the number of classes, the
number of layers, the image shape and the type of data that this image is
composed of.

\inputminted[firstline=33, lastline=54, linenos=true]{python}{../src/run_tvm.py}

At the moment, it implements only \texttt{resnet} and \texttt{mlp} networks
workloads but it can be extended to the other workloads that NNVM supports. \\

The function \mintinline{python}{tune_tasks()} is responsible for data layout
optimisation. \\
Data layout optimisation converts a computational graph into one that can use
better internal data layouts for execution on the target hardware. The function
will not be discussed here since its implementation is not relevant.

One thing to know is that the best tuning configuration is saved into a file.
Once the tuning has been done, it is not required to do it every time.\\

The function \mintinline{python}{tune_and_evaluate()} is the most important since
it is responsible for calling all the other functions and evaluating the inference.

\inputminted[firstline=91, lastline=153, linenos=true]{python}{../src/run_tvm.py}

On line 94, we retrieve the network workload (the \mintinline{python}{network_opt}
variable will be discussed later).

Then, if the \mintinline{python}{do_tuning} parameter is set to \mintinline{python}{True},
tuning tasks are extracted from the graph on given symbols and data layout
optimisation is performed.

On line 113, the best optimisations are picked and applied during compilation.
Once the compilation is done (line 116), the library containing the module is
uploaded to the target device (CPU if \mintinline{python}{env.TARGET == 'sim'},
Pynq board if \mintinline{python}{env.TARGET == 'pynq'}).

Then, we create random data that fits the network input shape and send it to the
device. The inference is then run multiple times (in the code, 100 times) and a mean inference
time and the standard deviation is displayed.

Once the TVM module has been compiled, it is sent to \acrfull{vta}, TVM's own hardware
accelerator. \acrshort{vta} is an open, generic and customisable deep learning
accelerator design. It's an end-to-end solution that includes drivers, a \acrshort{jit}
runtime and an optimising compiler stack based on TVM.

This generic deep learning accelerator is built around a GEMM core, which performs
dense matrix multiplication at a high computational throughput.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.8\linewidth]{figures/vta_overview.png}
    \captionof{figure}{High-level overview of the \acrshort{vta} hardware organisation \cite{vta}.}
    \label{fig:vta_overview}
\end{minipage}

The figure \ref{fig:vta_overview} presents a high-level overview of the \acrshort{vta}
hardware organisation. \acrshort{vta} is composed of four modules that communicate
between each other via FIFO queues and single-writer/single-reader SRAM memory
blocks, to allow for task-level pipeline parallelism. The compute module performs
both dense linear algebra computation with its GEMM core, and general computation
with its tensor ALU.

The \acrshort{vta} hardware design template offers modularity to the user, with
the option to modify hardware datatypes, memory architecture, the GEMM core
dimensions, hardware operators and pipelining stages.

\subsubsection{Results}

For comparison purposes, the model has been run on an AMD Ryzen 3 1300 Quad-Core CPU and
on the Pynq board.

\subsubsection*{MLP}

\textbf{CPU}

\begin{minted}{text}
Mean inference time (std dev): 0.10 ms (0.69 ms)
\end{minted}

\textbf{Pynq board}

\begin{minted}{text}
Mean inference time (std dev): 0.39 ms (0.02 ms)
\end{minted}

\subsubsection*{ResNet-50}

\textbf{CPU}

\begin{minted}{text}
Mean inference time (std dev): 107.25 ms (36.61 ms)
\end{minted}

\textbf{Pynq board}

\begin{minted}{text}
Mean inference time (std dev): 9190.38 ms (107.43 ms)
\end{minted}

We see that for each model, the \acrshort{cpu} achieves better results than the
\acrshort{fpga}. It might be due to the fact that the Pynq board processor is
running at 650 MHz while the CPU used for testing has a frequency of 3.5 GHz.

To have a meaningful comparison, we should compare the number of operations
both technologies perform for the same \acrshort{nn} model and not just the
time of execution (which depends on a lot of factors). Unfortunately, we didn't
find the time to measure the number of operations performed by each device.

\subsubsection{Summary}

TVM/VTA installation is pretty straightforward. The Python frontend is easy to
use and a lot of tutorials are here to help the developer to implement what he
wants.

The more the \acrshort{nn} model is complex, the more the tensors tuning will
take time and time required for loading the module on the \acrshort{fpga} will
also increase (about 15 minutes for ResNet-50 model).

Loading the \acrshort{nn} model on the \a

In conclusion, TVM is good framework to use for running inference on \acrshortpl{fpga}
and its quality will increase with future releases without any doubt.

\subsection{LeFlow}

\subsubsection{Installation}

Because LeFlow relies on the LegUp tool, the first thing is to install it. LegUp
provides an official virtual machine with everything already setup. The virtual
machine is available on the \textit{LegUp Download} page\footnotemark (22.8 GB).
We recommend using VirtualBox to run the virtual machine since it is free.

\footnotetext{\url{http://legup.eecg.utoronto.ca/download.php}}

Once the virtual machine has been downloaded and is running, we need to install
LeFlow. To do so, simply clone the repository:

\begin{minted}{bash}
git clone https://github.com/danielholanda/LeFlow
\end{minted}

Because LeFlow makes some minor changes on Tensorflow, we need to install a
modified version of Tensorflow:

\begin{minted}{bash}
cd LeFlow
sudo apt-get install python-pip
sudo python -m pip install --upgrade pip
cd src/tensorflow
sudo pip install tensorflow-1.6.0-cp27-cp27mu-linux_x86_64.whl --ignore-installed six
\end{minted}

If you didn't download the LegUp virtual machine, you must check that the configuration
variables \texttt{python\_path} and \texttt{legup\_examples\_folder} in the
\texttt{src/LeFlow} file point to the right paths.

It is also important to make the \texttt{src/LeFlow} file an executable:

\begin{minted}{bash}
chmod +x src/LeFlow
\end{minted}

If you are interested in testing the installation, you can do so by executing the
following commands:

\begin{minted}{bash}
cd test
python test_all.py --fast
\end{minted}

All the tests should take less than a minute.

\subsubsection{Implementation}

LeFlow only needs the value of all weights and biases of the model to map them
to on-chip memories.

There are two ways for retrieving weights and biases of a neural network:

\begin{enumerate}
\item   Locally train the network
\item   Download a pre-trained network
\end{enumerate}

Since ResNet-50 is quite a big network, the first alternative would take a lot of
time (potentially several days on a common computer).

Unfortunately, the second alternative didn't work either. The only pre-trained
official networks are not compatible with the version of Tensorflow that LeFlow
uses.

Given this issue, we could not implement the ResNet-50 model using LeFlow. However,
a simple MLP network can be trained and implemented on an Altera DE1-SoC board.

The code used to train and implement the MLP network is in the file \texttt{src/run\_leflow.py}

To generate hardware for the MLP network, we need to execute the following commands:

\begin{minted}{bash}
cd src/

# Generate hardware
/path/to/LeFlow/src/LeFlow run_leflow.py

# Test on Modelsim
make v -C run_leflow_files/
\end{minted}

Using the Quartus software (already installed on the virtual machine), we can upload
the design to the board.

For generating hardware, LeFlow relies on LegUp. LeFlow passes the C++ code (got
from the Tensorflow Python description of the network) to LegUp and the latter
performs the generation.

Most of the LegUp code is implemented as a target backend pass in the LLVM
compiler framework. The LegUp code is logically structured according to the flow
chart in the figure \ref{fig:legup_flow}.

\begin{minipage}{\linewidth}
    \centering
    \includegraphics[width=0.7\linewidth]{figures/legup_flow.png}
    \captionof{figure}{LegUp flow \cite{legup-progman}.}
    \label{fig:legup_flow}
\end{minipage}

There are five major logical steps performed in order: Allocation, Scheduling,
Binding, RTL generation and producing Verilog output.

During the Allocation step, LegUp reads in a user Tcl configuration script that
specifies the target device, timing constraints and \acrshort{hls} options.
Another Tcp script that contains the \acrshort{fpga} device specific operation
delay and area characteristics is read. This step also handles mapping LLVM
instructions to unique signal names in Verilog and ensuring these names do not
overlap with reserved Verilog keywords.

The next step (Scheduling) loops over each function in the program and performs
\acrshort{hls} scheduling.

The Binding step performs bipartite weighted matching. Binding results are stored
in a data structure that maps each LLVM instruction to the name of the hardware
functional unit that the instruction should be implemented on.

In the RTL Generation step, each LLVM instruction is looped over and LegUp creates
an RTL data structure, which represents the final circuit, based on the Scheduling
and Binding information.

Finally, LegUp loops over each RTL data structure previously created and prints
out the corresponding Verilog for the hardware module.

\subsubsection{Results}

When trying to run the file \texttt{run\_leflow.py} with LeFlow, the framework
outputs an error:

\begin{minted}{text}
legup@legup-vm:~/LeFlow/examples/PA$ ../../src/LeFlow run_leflow.py
INFO: Creating project...
INFO: Cleaning previous files...
INFO: Generating IR from tensorflow...
INFO: Cleaning unused dumped files...
Traceback (most recent call last):
  File "../../src/LeFlow", line 200, in <module>
    run_leflow(args.file_name,tool_path)
  File "../../src/LeFlow", line 79, in run_leflow
    shutil.copy(project_folder+"ir/ir-cluster_0__XlaCompiledKernel_true__
      XlaNumConstantArgs_0__XlaNumResourceArgs_0__module-with-opt.ll",
      project_folder+project_name+"_ir_1.ll")
  File "/usr/lib/python2.7/shutil.py", line 119, in copy
    copyfile(src, dst)
  File "/usr/lib/python2.7/shutil.py", line 82, in copyfile
    with open(src, 'rb') as fsrc:
IOError: [Errno 2] No such file or directory:
  'run_leflow_files/ir/ir-cluster_0__XlaCompiledKernel_true__
  XlaNumConstantArgs_0__XlaNumResourceArgs_0__module-with-opt.ll'
\end{minted}

This error is due to the fact that XLA, the Tensorflow compiler, generates
vectorized instructions which are not supported by LegUp \cite{github-leflow-issue}.

Because of this issue, we were unable to compile the simple \acrshort{mlp} and
make it run on an \acrshort{fpga}.

\subsubsection{Summary}

LeFlow is a good framework to use if you know what you are doing. The two main
issues are the lack of documentation, which is inexistant; and the fact that
it relies on the LegUp tool, which comes with its own limits and issues.