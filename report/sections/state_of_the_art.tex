\section{Existing frameworks}

This section reviews existing frameworks that aim at translating a \acrlong{nn}
model to synthesizable hardware.

The following criteria will be evaluated for each tool:

\begin{enumerate}
\item   Small description, tool's objectives
\item   Maintenance and Update frequency -- It is important to know if the tool
        is under active development and who is responsible for the development.
\item   License -- License is important since we might want to modify the tool so
        it better suits our needs.
\item   Which input(s) the tool accepts -- Does it use already existing \acrshort{ml}
        description framework(s) or its own?
\item   Which output(s) the tool targets -- The more outputs a tool supports, the
        more it is an interesting candidate.
\item   Remarks, if needed
\end{enumerate}

Once each tool has been reviewed, we will pick the most interesting candidates
and focus on evaluating their performances.

A summary of the review is available in the section \ref{sec:sota-summary}.

\subsection{DnnWeaver 2.0}

DnnWeaver is an open-source framework for accelerating \acrfullpl{dnn} on
\acrshortpl{fpga}. It aims to bridge the semantic gap between the high-level
specifications of \acrshort{dnn} models used by programmers and \acrshort{fpga}
acceleration \cite{dnnweaver}.

\subsubsection*{Maintenance and Update frequency}

DnnWeaver is maintained by a team of six developers who are part of the
Georgia Institute of Technology.

The project doesn't seem to be updated frequently. Based on its Github repository
\cite{dnnweaver-repo}, some commits occurred in April 2019 but the previous ones
were made in November 2018.

The fact that the project is maintained by a scholar team probably means that
they have other works besides DnnWeaver and it reflects on the commits history.

\subsubsection*{License}

According to its website, DnnWeaver is licensed under the Apache License 2.0
\cite{license-apache2}, which requires preservation of the copyright notice and
disclaimer but allows the user of the software the freedom to use the software
for any purpose, to distribute it, to modify it and to distribute modified
versions of the software, under the terms of the license, without concerns for
royalties.

\subsubsection*{Input}

The programmer specifies the \acrlong{dnn} using Caffe format.

\subsubsection*{Output}

Given the input, the framework automatically generates the accelerator Verilog
code specialised for the given network, using hand-optimised Verilog templates
(included in DnnWeaver).

As of March 2019, the implemented layers are Convolution, \acrfull{relu}, Fully
Connected Layer (InnerProduct), Pooling and \acrfull{lrn}.

\subsubsection*{Remarks}

The available documentation concerns the version 1.0 of the tool, which make it
difficult to gather valid information about the current version.

A colleague has used DnnWeaver in the past and, according to him, the framework
uses a mix of \textit{Python2.7} and \textit{Python3.6}, raising errors during
utilisation. The only fix is to update manually incompatible files to
\textit{Python3.6}.

Additionally, it seems that adding an \acrshort{fpga} unknown to the framework as
an output target requires a lot of efforts.

\subsection{FPGA Caffe}

FPGA Caffe is a custom version of Caffe with FPGA kernels. The kernels use
custom-precision floating-point arithmetic to save area and improve the
throughput of the kernels, while also allowing for experimentation with different
floating-point precisions and rounding for training and inference with
\acrshortpl{cnn} \cite{fpga-caffe}.

\subsubsection*{Maintenance and Update frequency}

FPGA Caffe is maintained by a team of six developers who are part of University
of Toronto and University of Guelph, both based in Ontario, Canada.

The project has not been updated since December 2017, making it quite obsolete.

\subsubsection*{License}

FPGA Caffe is released under the 2-Clause BSD License \cite{license-bsd2}, which
allows almost unlimited freedom with the software as long as the modified versions
of the software include the same license.

\subsubsection*{Input}

The programmer specifies the \acrlong{dnn} using Caffe format.

\subsubsection*{Output}

The kernels target the Xilinx SDAccel OpenCL environment, thus only Xilinx
\acrshortpl{fpga} are supported.

The implemented layers are: forward- and backward-Convolution, forward- and
backward-\acrshort{relu}, forward- and backward-MaxPooling, and forward- and
backward-InnerProduct.

\subsubsection*{Remarks}

None.

\subsection{NVDLA}

The \acrfull{nvdla} is a free and open architecture that promotes a standard way
to design Deep Learning inference accelerators \cite{nvdla}. With its modular
architecture, \acrshort{nvdla} is scalable, highly configurable and designed to
simplify integration and portability. The hardware supports a wide range of IoT
devices. According to their website, all of the software, hardware and documentation
\textit{will} be available on GitHub \cite{nvdla-github}.

\subsubsection*{Maintenance and Update frequency}

The project is maintained by an internal team so it's quite professional.

\acrshort{nvdla} is divided in two parts: software and hardware. \\
The hardware part (available under the \texttt{hw} Github repository) has not
been updated since April 2018. \\
The software part (available under the \texttt{sw} Github repository) has
multiple commits since April 2019 but nothing between April 2019 and August 2018
(at least publicly). \\
It seems that they commit changes only when a major development milestone is
released.

\subsubsection*{License}

\acrshort{nvdla} is delivered as an open-source project under the NVIDIA Open
NVDLA License \cite{nvdla-license}.

\subsubsection*{Input}

The programmer specifies the \acrlong{dnn} using Caffe format.

\subsubsection*{Output}

\acrshort{nvdla} supports two sample platforms: simulation and \acrshort{fpga}.
These platforms are provided to observe, evaluate and test \acrshort{nvdla} in a
minimal \acrfull{soc}. \\
The simulation platform is based on GreenSocs QBox \cite{greensocs}. A QEMU CPU
model (\texttt{x86} or \texttt{ARMv8}) is combined with the \acrshort{nvdla} SystemC
model to provide a register-accurate system for quick development and debugging. \\
The FPGA platform provides a synthesizable example of instantiating \acrshort{nvdla}
in a real design. The \acrshort{fpga} model is intended for inference only, no
effort has been made to optimise cycle time, design size, power consumption or
performance.

The FPGA platform is Xilinx-based, thus only Xilinx \acrshortpl{fpga} are supported.

\subsubsection*{Remarks}

The documentation is well-structured and precise.

The source code is quite closed yet. Commits are pushed whenever there is a major
version. And even if NVIDIA claims that the project is open-source, it seems that
some code (\textit{e.g.} the compiler) will not be released. This already causes
problems because, according to Toshiba \cite{toshiba}, some errors are raised during
compilation, and we don't have information on what is making compilation fail.

It also seems that people are having a hard time testing \acrshort{nvdla} on
\acrshortpl{fpga}\footnotemark.

The team seems to be quite responsive: we wrote them an email to get more
information about the compiler source code and they replied within hours.

\footnotetext{\url{https://github.com/nvdla/hw/issues/110}}

\subsection{hls4ml}

hls4ml is a package for Machine Learning inference in \acrshortpl{fpga}.
It translates traditional open-source Machine Learning models into \acrfull{hls}
language that can be configured according to the output platform \cite{hls4ml}.

\subsubsection*{Maintenance and Update frequency}

hls4ml is maintained by different people around the world. Some come from the
CERN, others from MIT, making it more of a community project than something
professional.

The project is updated quite frequently, about 1 commit is made per week.

\subsubsection*{License}

hls4ml is licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Input}

\acrlong{nn} can be specified with Keras/Tensorflow or PyTorch.

\subsubsection*{Output}

Given the input, the framework generates an \acrshort{hls} project that can be
used to produce an IP core which can be plugged into more complex designs or be
used to create a kernel for \acrshort{cpu} co-processing.

As of May 2019, only \acrfull{mlp} and Conv1D/Conv2D architectures are supported.

\subsubsection*{Remarks}

The documentation doesn't give a lot of information and was last updated a year
ago.

\subsection{TVM}

TVM is an open deep learning compiler stack for \acrshortpl{cpu}, \acrshortpl{gpu}
and specialised accelerators (\acrshortpl{fpga}). It aims to close the gap
between the productivity-focused deep learning frameworks, and the performance-
or efficiency-oriented hardware backends \cite{tvm}.

\subsubsection*{Maintenance and Update frequency}

The TVM stack began as research project at the SAMPL group of University of
Washington. The project is now driven by an open-source community involving
multiple industry and academic institutions.

The project is under active development: several commits are pushed every day.

\subsubsection*{License}

The TVM stack is licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Input}

\acrlong{nn} can be specified in Keras, MXNet, PyTorch, Tensorflow, CoreML and
DarkNet.

\subsubsection*{Output}

Given the input, TVM compiles the \acrfull{dl} models into minimum deployable
modules on diverse hardware backends such as \acrshortpl{cpu}, \acrshortpl{gpu}
and \acrshortpl{fpga}.

\subsubsection*{Remarks}

The documentation is really good and up-to-date.

A lot of tutorials are available for every thing that TVM can achieve.

A forum is available where users can ask question and questions are rapidly
answered.

\acrfull{vta} is an extension of the TVM framework that includes drivers, a
\acrfull{jit} runtime and an optimising compiler stack. This extension is
currently specified in Vivado HLS C++ which is only supported by Xilinx
toolchains. However, a pull request adding Intel \acrshort{fpga} and Chisel
support is currently being reviewed \cite{tvm-intel-pr}.

\subsection{LeFlow}

LeFlow is a tool that relies on LegUp \cite{legup} to map numerical computation
models written in Tensorflow to synthesizable hardware \cite{leflow}. It bridges
Google's XLA compiler and LegUp high-level synthesis to automatically generate
Verilog.

\subsubsection*{Maintenance and Update frequency}

LeFlow is maintained by three people who work at The University of British
Columbia.

The project was last updated in February 2019, which is not that old but before
that, the last update is from November 2018 so it is not updated frequently.

\subsubsection*{License}

LeFlow is licensed under the MIT License \cite{license-mit} which has only one
restriction: if the project is reused within proprietary software, all copies of
the licensed software must include a copy of the MIT License.

\subsubsection*{Input}

\acrlong{nn} models must be specified with a customised version of Tensorflow.

\subsubsection*{Output}

Since LeFlow relies on LegUp, it supports the output that LegUp supports. Those
are Intel \acrshortpl{fpga}.

\subsubsection*{Remarks}

Documentation is inexistant.

Some simple examples are included in the repository.

The maintainer is quite responsive: we wrote him an email to better understand
the project's structure and he replied within hours.

\subsection{nGraph}

nGraph is an open-source graph compiler for artificial \acrlongpl{nn}. The
nGraph Compiler stack provides an inherently efficient graph-based compilation
infrastructure designed to be compatible with many upcoming integrated circuits
while also unlocking a massive performance boost on any existing hardware targets
\cite{ngraph}.

\subsubsection*{Maintenance and Update frequency}

nGraph is maintained by an artificial intelligence software company called
Nervana Systems (acquired by Intel in August 2016) \cite{wiki-nervanasys}.

The Github repository is updated with several commits every day \cite{github-ngraph}.

\subsubsection*{License}

nGraph is licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Input}

As of May 2019, nGraph takes Tensorflow and MXNet as inputs for model description.

\subsubsection*{Output}

nGraph currently supports Intel \acrshortpl{cpu}, Intel Neural Network Processor,
NVIDIA CUDA \acrshortpl{gpu} and AMD \acrshortpl{gpu} as output.

FPGA are set to be fully supported but no time information is given.

\subsubsection*{Remarks}

nGraph released a Release Candidate version on the 24\textsuperscript{th} of
May 2019 with documentation. Since the release was too close to the end of this
study, we could not investigate more on this framework.

The documentation seems to be very qualitative.

Since the project is maintained by an Intel company, it might be possible that
only Intel hardware (\acrshortpl{cpu}, \acrshortpl{gpu} and \acrshort{fpga}) will
be supported in the future.

\subsection{HeteroCL}

HeteroCL is a programming infrastructure composed of a Python-based \acrfull{dsl}
and a compilation flow. The HeteroCL \acrshort{dsl} provides a clean abstraction
that decouples algorithm specification from three important types of hardware
customisation in compute, data types and memory architectures.

\subsubsection*{Maintenance and Update frequency}

The project is maintained by a team of developers of the Cornell University.

Commits are pushed frequently on the HeteroCL Github repository \cite{github-heterocl}.

\subsubsection*{License}

HeteroCL is licensed under the Apache License 2.0 \cite{license-apache2}.

\subsubsection*{Input}

HeteroCL takes PyTorch, MXNet and Keras \acrshort{nn} specifications as inputs.

\subsubsection*{Output}

Cloud (AWS), Xilinx and Intel \acrshortpl{fpga} are supported. \acrshortpl{cpu} are
also supported.

\subsubsection*{Remarks}

HeteroCL released its first version, v0.1, just days before the end of this study.
Thus, we could not investigate more on this framework. It's unfortunate because
it seems to be the most interesting framework since it offers more outputs than any
other.

The documentation seems to be good although not complete. We assume it will
improve with future releases.

It is possible to follow development roadmap by going on their respective pull
request on the HeteroCL Github repository \cite{github-heterocl}.

\subsection{Comparison}

\underline{Reminder:} A summary of the reviewed frameworks is available at the
section \ref{sec:sota-summary}.

Now that the first objective (review existing frameworks) has been completed, we
can move on to the second, which is picking the most interesting candidates.

We want to pick at least two candidates so that we can do a comparison of their
respective performance. \\
The selection will be based on several criteria: we want to focus on the update
frequency and the supported outputs. The \textit{License} criterion has been put
aside because every framework is under a license which allows a lot of freedom
(except maybe for \acrshort{nvdla}). \\
Also, the \textit{Supported inputs} criterion has not been taken into account
since input frameworks are similar and switching from one to another is quite
feasible.

nGraph is not an interesting candidate yet since it doesn't support \acrshortpl{fpga}
as output yet. \\
Unfortunately, we could not retain HeteroCL as a candidate because it has been
released just days before the end of this study. But it might be a truly
interesting framework to follow in the future. \\
Because its compiler is still close-source and contains bugs, \acrshort{nvdla}
didn't make it to the list of interesting candidates. \\
DnnWeaver 2.0 seems to be broken: some files need manual updates to be compatible
with \textit{Python3.6} so it has been pulled out of the interesting candidates. \\
FPGA Caffe has not been updated for more than a year, so it obviously is not an
interesting candidate.

We have three candidates left: hls4ml, TVM and LeFlow. LeFlow is the only framework
to support Intel \acrshortpl{fpga} as output so we are keeping it as an interesting
candidate. \\
Between hls4ml and TVM, we chose to go with TVM because the update frequency is
better by far as well as the documentation.

In conclusion, the two most interesting candidates to be retained are LeFlow and
TVM.

\begin{landscape}

\subsection{Summary}
\label{sec:sota-summary}

\begin{center}
\begin{tabularx}{0.95\linewidth}{|l|L|L|l|L|}
\hline
    \textbf{Framework}          &
    \textbf{Update frequency}   &
    \textbf{Supported inputs}   &
    \textbf{Supported outputs}  &
    \textbf{Comments}           \\
\hline
    DnnWeaver 2.0 &
    One update every year (last in April 2019) &
    Caffe &
    Verilog &
    Obsolete documentation, some files need to be updated manually for the
    framework to run. \\
\hline
    FPGA Caffe &
    Not updated since December 2017 &
    Caffe &
    Xilinx \acrshortpl{fpga} &
    None.\\
\hline
    NVDLA &
    One update every year (last in April 2019) &
    Caffe &
    \acrshort{cpu}, Xilinx \acrshort{fpga} &
    Documentation is good. Project is \textit{relatively} open-source. Seems that
    people are having a hard time deploying on \acrshortpl{fpga}. \\
\hline
    hls4ml &
    1 commit per week &
    Keras, PyTorch &
    Synthesizable IP Core &
    Documentation last updated a year ago. \\
\hline
    TVM &
    Several commits per day &
    Keras, MXNet, PyTorch, Tensorflow, CoreML, DarkNet &
    \acrshortpl{cpu}, \acrshortpl{gpu}, Xilinx \acrshortpl{fpga} &
    Good documentation with tutorials. Have a forum for asking questions. A pull
    request is being reviewed for integrating Intel \acrshortpl{fpga} support. \\
\hline
    LeFlow &
    Last updated in February 2019 and November 2018 &
    Tensorflow &
    Intel \acrshortpl{fpga} &
    No documentation. Some examples are included. \\
\hline
    nGraph &
    Several commits per day &
    Tensorflow, MXNet &
    \acrshortpl{cpu}, \acrshortpl{gpu} &
    \acrshortpl{fpga} not supported yet. \\
\hline
    HeteroCL &
    Several commits per week &
    PyTorch, MXNet, Keras &
    \acrshortpl{fpga}, \acrshortpl{cpu} &
    The framework was released just day before the end of this study thus we
    had no time to investigate further. \\
\hline
\end{tabularx}
    \captionof{table}{Frameworks summary.}
\end{center}

\end{landscape}